import React, { Component } from 'react';
import Cita from './Cita'
import PropTypes from 'prop-types'
import axios from 'axios'
class ListaCitas extends Component {
    state = {
        symptons: []
    }
    componentWillMount() {

        axios.get('http://127.0.0.1:8000/symptons')
            .then(res => {
                this.setState({
                    symptons: res.data
                })
            })
      }
    render() {
        const citas = this.props.citas
        return (
            <div className="card mt-5">
                <div className="card-body">
                    <h2 className="card-title text-center"> {this.props.mensaje}</h2>
                    <div className="lista-citas">
                        {citas.map(cita => (
                            <Cita 
                                key = {cita.id}
                                cita = {cita}
                                borrarCita = {this.props.borrarCita}
                                symptons = {this.state.symptons}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}
ListaCitas.propTypes = {
    citas: PropTypes.array.isRequired,
    borrarCita: PropTypes.func.isRequired,
    mensaje: PropTypes.string.isRequired
}
export default ListaCitas;