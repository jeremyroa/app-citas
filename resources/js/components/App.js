import axios from 'axios';
import React, { Component } from 'react';
import Header from './Header'
import AgregarCita from './AgregarCita'
import ListaCitas from './ListaCitas'

class App extends Component {

  state = {
    citas: []
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/patient')
        .then(res => {
          this.setState({
            citas: res.data ? res.data : []
          })
        })

  }

  // componentDidUpdate() {
  //   axios.get('http://127.0.0.1:8000/patient')
  //       .then(res => {
  //         this.setState({
  //           citas: res.data ? res.data : []
  //         })
  //     })
  // }

  crearCita = nuevaCita => {
    axios.post('http://127.0.0.1:8000/patient-save',nuevaCita)
        .then((response)=>{
            console.log(response)
        }).catch((error)=>{
            console.log(error.response.data)
        })
  }
  
  borrarCita = id => {
    axios.delete(`http://127.0.0.1:8000/patient/${id}`,{params: {id}})
        .then(r => console.log(r))
        .catch(e => console.log(e))
    let citas = this.state.citas
    citas = citas.filter(cita => cita.id !== id)

    this.setState({
      citas
    })
  }

  render() {
    return (
      <div className="container">
        <Header 
          titulo = {'Administrador de Pacientes de Veterinaria'}
        />
        <main className="row">
          <div className="col-12 col-lg-7">
              <AgregarCita 
                crearCita = {this.crearCita}
              />
          </div>
          <div className="col-12 col-lg-5">
            <ListaCitas 
              citas = {this.state.citas}
              mensaje = {this.hayCitas()}
              borrarCita = {this.borrarCita}
            />
          </div>
        </main>
      </div>
    );
  }

  hayCitas() {
    return this.state.citas.length === 0 ? 'No hay citas' : 'Administra tus citas aqui'
  }
}

export default App;
