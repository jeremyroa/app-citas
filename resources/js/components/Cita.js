import React, { Component } from 'react';
import PropTypes from 'prop-types'
import axios from 'axios';

class Cita extends Component {


    borrarCita = () => {
        this.props.borrarCita(this.props.cita.id)
    }
    render() {
        const {name,owner_name,symptons_id,__sympton}= this.props.cita
        const r = this.props.symptons
        return (
            <div className="media mt-3">
                <div className="media-body">
                    <h3 className="mt-0">{name}</h3>
                    <p className="card-text"><span>Nombre del dueño: </span> {owner_name}</p>
                    {/* <p className="card-text"><span>Fecha: </span> {fecha}</p>
                    <p className="card-text"><span>Hora: </span> {hora}</p> */}
                    <p className="card-text"><span>Sintomas: </span></p>
                    <p className="card-text">{r.filter(e => e.id == symptons_id)[0].name}</p>
                    <button onClick={this.borrarCita} className="btn btn-danger">
                        Borrar &times;
                    </button>
                </div>
            </div>
        );
    }
}
Cita.propTypes = {
    cita: PropTypes.shape({
        // fecha: PropTypes.string.isRequired,
        // hora: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        owner_name: PropTypes.string.isRequired,
        symptons_id: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired
    }).isRequired,
    borrarCita: PropTypes.func.isRequired
}
export default Cita;