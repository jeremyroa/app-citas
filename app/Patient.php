<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = ['name','owner_name','symptons_id'];
    public function __sympton()
    {
        return $this->belongsTo('App\Sympton','symptons_id','id');
    }

}
