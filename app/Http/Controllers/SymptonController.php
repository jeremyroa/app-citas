<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sympton;

class SymptonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Sympton::all();
    }
}
