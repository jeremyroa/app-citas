<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sympton extends Model
{
    protected $table = 'symptons';
    protected $fillable = ['name'];
}
