<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(1),
        'owner_name' => $faker->name,
    ];
});
