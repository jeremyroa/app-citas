<?php

use Illuminate\Database\Seeder;
use App\Patient;
use App\Sympton;

class PatientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $symptons_id = Sympton::where('name', 'Gripe')
        ->value('id');

        Patient::create([
            'name' => 'Doki',
            'owner_name' => 'Jeremy',
            'symptons_id' => $symptons_id
        ]);

        factory(Patient::class,3)->create([
            'symptons_id' => $symptons_id
        ]);
    }
}
