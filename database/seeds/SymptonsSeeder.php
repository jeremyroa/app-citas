<?php

use Illuminate\Database\Seeder;
use App\Sympton;

class SymptonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sympton::create([
            'name' => 'Gripe'
        ]);
        Sympton::create([
            'name' => 'Herida'
        ]);
        factory(Sympton::class,3)->create();
    }
}
