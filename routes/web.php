<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','patient.index');
Route::get('/patient','PatientController@index');
Route::get('/symptons','SymptonController@index');
Route::post('/patient-save', 'PatientController@store');
Route::delete('/patient/{id}', 'PatientController@destroy');
